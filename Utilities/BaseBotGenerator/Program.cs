﻿// See https://aka.ms/new-console-template for more information

using BotCore.DataModel;

var basedir = "../../../../../";
Console.WriteLine("Base dir: " + new DirectoryInfo(basedir).FullName);
var lines = File.ReadAllLines(basedir + "/DiscordBots.sln").ToList();
Console.WriteLine(lines.IndexOf("Global"));

var db = Db.GetNewPostgres();
foreach (var dbBot in db.Bots)
{
    if (
        Directory.Exists(basedir + $"/Bots/Bots.{dbBot.Name.Replace(" ", "")}") ||
        Directory.Exists(basedir + $"/Bots/Bots.{dbBot.FriendlyName.Replace(" ", "")}")
    ) continue;
    var name = "Bots." + Ask($"Enter project name for {dbBot.FriendlyName}: Bots.");
    var guid = Guid.NewGuid();
    if (Directory.Exists(basedir + $"/Bots/{name}")) continue;
    Console.WriteLine($"Generating project for: {name}");
    Directory.CreateDirectory(basedir + $"/Bots/{name}");
    File.WriteAllText(basedir + $"/Bots/{name}/{name}.csproj", @"<Project Sdk=""Microsoft.NET.Sdk"">
    <PropertyGroup>
        <OutputType>Exe</OutputType>
        <TargetFramework>net6.0</TargetFramework>
        <ImplicitUsings>enable</ImplicitUsings>
        <Nullable>enable</Nullable>
    </PropertyGroup>
    <ItemGroup>
        <ProjectReference Include=""..\..\BotCore\BotCore.csproj"" />
    </ItemGroup>
</Project>");
    lines.Insert(1,
        $@"Project(""{{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}}"") = ""{name}"", ""Bots\{name}\{name}.csproj"", ""{{{guid}}}""
EndProject");
    lines.Insert(GetConfigs(), $@"		{{{guid}}}.Debug|Any CPU.ActiveCfg = Debug|Any CPU
		{{{guid}}}.Debug|Any CPU.Build.0 = Debug|Any CPU
		{{{guid}}}.Release|Any CPU.ActiveCfg = Release|Any CPU
		{{{guid}}}.Release|Any CPU.Build.0 = Release|Any CPU");
    lines.Insert(GetNestedProjs(), $"\t\t{{{guid}}} = {{72206BAC-1566-4F26-AD54-27EA6FF7CF3D}}");
    File.WriteAllLines(basedir + "DiscordBots.sln", lines);
    File.WriteAllText(basedir + "/Bots/" + name + "/Program.cs", $@"using BotCore.DataModel;
using BotCore.DbExtras;
using BotCore.Util.DbExtensions;

var db = Db.GetPostgres();
Bot bot = db.GetBot(""{dbBot.Name}"");
var bi = new BotImplementation(db, bot);
bi.Init(db, bot: bot);

Thread.Sleep(int.MaxValue);");
}

string Ask(string text)
{
    Console.Write(text);
    return Console.ReadLine();
}

int GetSolutionListIndex()
{
    return 1;
}

int GetConfigs()
{
    return lines.IndexOf("\tGlobalSection(ProjectConfigurationPlatforms) = postSolution") + 1;
}

int GetNestedProjs()
{
    return lines.IndexOf("\tGlobalSection(NestedProjects) = preSolution") + 1;
}