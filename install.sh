#!/bin/sh
if [ "$EUID" -ne 0 ]
  then echo "Please run as root, also make sure root can read/write these files and folders!"
  exit
fi
if [ ! -f "DiscordBots.sln" ]; then
  echo "You must run this script from the project directory!"
  exit
fi

dotnet run --project Utilities/InstallScriptGenerator --start --enable | bash