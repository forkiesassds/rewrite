using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Logging;

namespace Bots.BinSh.DbModel;

public class RoleDbDtf : IDesignTimeDbContextFactory<RoleDb>
{
    public RoleDb CreateDbContext(string[] args)
    {
        var rcfg = RoleDbConfig.Read();
        rcfg.Save();
        return new RoleDb(new DbContextOptionsBuilder<RoleDb>()
            .UseNpgsql(
                $"Host={rcfg.Host};Database={rcfg.Database};Username={rcfg.Username};Password={rcfg.Password};Port={rcfg.Port};Include Error Detail=true")
            .LogTo(Console.WriteLine, LogLevel.Information).EnableSensitiveDataLogging().Options);
    }
}