﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bots.BinSh.DbModel;

[Table("roles")]
public class RoleEntry
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int IntRoleId { get; set; }

    //properties
    public ulong DiscordServerId { get; set; }
    public ulong DiscordRoleId { get; set; }
    public ulong DiscordMessageId { get; set; }
}