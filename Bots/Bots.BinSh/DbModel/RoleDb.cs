﻿using Microsoft.EntityFrameworkCore;

namespace Bots.BinSh.DbModel;

public class RoleDb : DbContext
{
    public RoleDb(DbContextOptions<RoleDb> options)
        : base(options)
    {
    }

    public DbSet<RoleEntry> Roles { get; set; } = null!;

    // protected override void OnModelCreating(ModelBuilder modelBuilder)
    // {
    //     OnModelCreatingPartial(modelBuilder);
    // }
    //
    // private void OnModelCreatingPartial(ModelBuilder modelBuilder)
    // {
    //     throw new NotImplementedException();
    // }
}