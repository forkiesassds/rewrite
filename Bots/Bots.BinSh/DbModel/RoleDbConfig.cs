using ArcaneLibs;

namespace Bots.BinSh.DbModel;

public class RoleDbConfig : SaveableObject<RoleDbConfig>
{
    public string Host { get; set; } = "localhost";
    public string Username { get; set; } = "postgres";
    public string Password { get; set; } = "postgres";
    public string Database { get; set; } = "binshroles";
    public short Port { get; set; } = 5432;
}