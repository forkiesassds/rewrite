﻿// See https://aka.ms/new-console-template for more information

using BotCore.DataModel;
using BotCore.DbExtras;
using BotCore.Util.DbExtensions;
using Bots.BinSh.Commands;
using Bots.BinSh.DbModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

var db = Db.GetPostgres();
var bot = db.GetBot("binsh");
var rcfg = RoleDbConfig.Read();
rcfg.Save();
var roleDb = new RoleDb(new DbContextOptionsBuilder<RoleDb>()
    .UseNpgsql(
        $"Host={rcfg.Host};Database={rcfg.Database};Username={rcfg.Username};Password={rcfg.Password};Port={rcfg.Port};Include Error Detail=true")
    .LogTo(Console.WriteLine, LogLevel.Information).EnableSensitiveDataLogging().Options);
roleDb.Database.Migrate();
roleDb.SaveChanges();
roleDb.Dispose();

var bi = new BotImplementation(db, bot);
new BotImplementation(db, bot);
bi.CommandManager.RegisterCommandsInType(typeof(Roles));

Thread.Sleep(int.MaxValue);