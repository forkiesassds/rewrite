﻿// See https://aka.ms/new-console-template for more information

using BotCore.DataModel;
using BotCore.DbExtras;
using BotCore.Util.DbExtensions;

var db = Db.GetPostgres();
var bot = db.GetBot("kinobot");
var bi = new BotImplementation(db, bot);

Thread.Sleep(int.MaxValue);