namespace Bots.SugarcaneBot.DataStructures.Timings.timings.aikar.co.Recommendations;

public class AikarSystemRecommendations
{
    public static List<TimingsRecommendations> GetRecommendations(AikarTimingsRoot timings)
    {
        List<TimingsRecommendations> recommendationsList = new();
        if (!timings.TimingsMaster.System.Flags.Contains("-Daikars.new.flags=true"))
            recommendationsList.Add(new TimingsRecommendations
            {
                title = "Use Aikar's flags",
                description = "We recommend that you use Aikar's flags!"
            });

        if (timings.TimingsMaster.System.Maxmem / 1024 / 1024 / 1024 < 2)
            recommendationsList.Add(new TimingsRecommendations
            {
                title = "Increase memory",
                description = "We recommend that you use at least 2GB of memory!"
            });
        else if (timings.TimingsMaster.System.Maxmem / 1024 / 1024 / 1024 > 8)
            recommendationsList.Add(new TimingsRecommendations
            {
                title = "Decrease memory",
                description =
                    "We recommend that you reduce your amount of memory! Allocating too much memory can cause lag."
            });
        if (timings.TimingsMaster.System.Timingcost > 300)
            recommendationsList.Add(new TimingsRecommendations
            {
                title = $"High timing cost ({timings.TimingsMaster.System.Timingcost})",
                description =
                    "Your timing cost is high. This means your CPU is overloaded and/or slow. Reduce the amount of servers on this system or upgrade your CPU!"
            });
        if (timings.TimingsMaster.System.Cpu < 4)
            recommendationsList.Add(new TimingsRecommendations
            {
                title = $"Low CPU thread count ({timings.TimingsMaster.System.Timingcost})",
                description =
                    "We recommend you have at least 4 CPU threads so plugins/code with multithreading can work better!"
            });
        return recommendationsList;
    }
}