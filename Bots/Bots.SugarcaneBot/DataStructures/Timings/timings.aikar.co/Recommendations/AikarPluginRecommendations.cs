namespace Bots.SugarcaneBot.DataStructures.Timings.timings.aikar.co.Recommendations;

public class AikarPluginRecommendations
{
    private static readonly Dictionary<string, string> BadPlugins = new();
    private static readonly Dictionary<string, string> PurpurBadPlugins = new();

    public static List<TimingsRecommendations> GetRecommendations(AikarTimingsRoot timings)
    {
        if (BadPlugins.Count == 0) PopulateDicts();
        List<TimingsRecommendations> recommendationsList = new();
        var cfg = timings.TimingsMaster.Configs;
        var plugins = timings.TimingsMaster.Plugins;

        foreach (var pl in plugins.Keys)
        {
            if (BadPlugins.ContainsKey(pl))
                recommendationsList.Add(new TimingsRecommendations
                {
                    title = $"Remove plugin: {pl}",
                    description = BadPlugins[pl]
                });
            if (PurpurBadPlugins.ContainsKey(pl) && cfg.ContainsKey("purpur"))
                recommendationsList.Add(new TimingsRecommendations
                {
                    title = $"Remove plugin: {pl}",
                    description = PurpurBadPlugins[pl]
                });
        }

        return recommendationsList;
    }

    private static void PopulateDicts()
    {
        BadPlugins.Add("VillagerOptimiser",
            "You probably don't need VillagerOptimiser as Spigot already adds its features. See entity-activation-range in spigot.yml.");
        BadPlugins.Add("ClearLag", "Plugins that claim to remove lag actually cause more lag.");
        BadPlugins.Add("LagAssist",
            "LagAssist should only be used for analytics and preventative measures. All other features of the plugin should be disabled.");
        BadPlugins.Add("NoChunkLag", "Plugins that claim to remove lag actually cause more lag.");
        BadPlugins.Add("NoMobLag", "Plugins that claim to remove lag actually cause more lag.");
        BadPlugins.Add("ServerBooster", "Plugins that claim to remove lag actually cause more lag.");
        BadPlugins.Add("AntiLag", "Plugins that claim to remove lag actually cause more lag.");
        BadPlugins.Add("BookLimiter", "You don't need BookLimiter as Paper already fixes all crash bugs.");
        BadPlugins.Add("LimitPillagers", "You probably don't need LimitPillagers as Paper already adds its features.");
        BadPlugins.Add("StackMob", "Stacking mobs causes more lag.");
        BadPlugins.Add("Stacker", "Stacking mobs causes more lag.");
        BadPlugins.Add("MobStacker", "Stacking mobs causes more lag.");
        BadPlugins.Add("WildStacker", "Stacking mobs causes more lag.");
        BadPlugins.Add("UltimateStacker", "Stacking mobs causes more lag.");
        BadPlugins.Add("IllegalStack",
            "You probably don't need IllegalStack as Paper already fixes all dupe and crash bugs.");
        BadPlugins.Add("ExploitFixer",
            "You probably don't need ExploitFixer as Paper already fixes all dupe and crash bugs.");
        BadPlugins.Add("EntityTrackerFixer", "You don't need EntityTrackerFixer as Paper already has its features.");
        BadPlugins.Add("Orebfuscator",
            "You don't need Orebfuscator as [Paper](https://gist.github.com/stonar96/ba18568bd91e5afd590e8038d14e245e) already has its features.");
        BadPlugins.Add("GroupManager",
            "GroupManager is an outdated permission plugin. Consider replacing it with [LuckPerms](https://luckperms.net/download).");
        BadPlugins.Add("PermissionsEx",
            "PermissionsEx is an outdated permission plugin. Consider replacing it with [LuckPerms](https://luckperms.net/download).");
        BadPlugins.Add("bPermissions",
            "bPermissions is an outdated permission plugin. Consider replacing it with [LuckPerms](https://luckperms.net/download).");
        BadPlugins.Add("PhantomSMP",
            "You probably don't need PhantomSMP as Paper already has its features. See phantoms-only-attack-insomniacs in paper.yml");
        BadPlugins.Add("PacketLimiter",
            "You don't need PacketLimiter as Tuinity (now merged with paper) already has its features.");
        //purpur
        PurpurBadPlugins.Add("SilkSpawners",
            "You probably don't need SilkSpawners as Purpur already has its features.");
        PurpurBadPlugins.Add("MineableSpawners",
            "You probably don't need MineableSpawners as Purpur already has its features.");
        PurpurBadPlugins.Add("VillagerLobotomizatornator",
            "You probably don't need VillagerLobotomizatornator as Purpur already adds its features. Enable villager.lobotomize.enabled in [purpur.yml](http://bit.ly/purpurc).");
    }
}