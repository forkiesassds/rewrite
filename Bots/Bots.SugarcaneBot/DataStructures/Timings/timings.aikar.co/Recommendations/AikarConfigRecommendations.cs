using Newtonsoft.Json.Linq;

namespace Bots.SugarcaneBot.DataStructures.Timings.timings.aikar.co.Recommendations;

public class AikarConfigRecommendations
{
    public static List<TimingsRecommendations> GetRecommendations(AikarTimingsRoot timings)
    {
        List<TimingsRecommendations> recommendationsList = new();
        var cfg = timings.TimingsMaster.Configs;
        var plugins = timings.TimingsMaster.Plugins;
        //add online mode if not bungee/velocity

        if (cfg.ContainsKey("bukkit"))
        {
            var bcfg = cfg["bukkit"];
            if (bcfg["chunk-gc"]["period-in-ticks"].ToObject<int>() > 600)
                recommendationsList.Add(new TimingsRecommendations
                {
                    title = "Chunk garbage collection",
                    description =
                        "Decrease chunk garbage collection time. We recommend using 400."
                });
            if (bcfg["ticks-per"]["monster-spawns"].ToObject<int>() == 1)
                recommendationsList.Add(new TimingsRecommendations
                {
                    title = "Mob spawns",
                    description =
                        "Increase the delay between mob spawns. We recommend using 4"
                });
            var sl = bcfg["spawn-limits"];
            if (sl["monsters"].ToObject<int>() > 70)
                recommendationsList.Add(new TimingsRecommendations
                {
                    title = "Spawn limits",
                    description =
                        "Decrease the spawn limit for monsters. We recommend using 15."
                });
            if (sl["water-ambient"].ToObject<int>() > 20)
                recommendationsList.Add(new TimingsRecommendations
                {
                    title = "Spawn limits",
                    description =
                        "Decrease the spawn limit for ambient water mobs. We recommend using 5."
                });
            if (sl["ambient"].ToObject<int>() > 15)
                recommendationsList.Add(new TimingsRecommendations
                {
                    title = "Spawn limits",
                    description =
                        "Decrease the spawn limit for ambient mobs. We recommend using 1."
                });
            if (sl["animals"].ToObject<int>() > 10)
                recommendationsList.Add(new TimingsRecommendations
                {
                    title = "Spawn limits",
                    description =
                        "Decrease the spawn limit for animals. We recommend using 5."
                });
            if (sl["water-animals"].ToObject<int>() > 15)
                recommendationsList.Add(new TimingsRecommendations
                {
                    title = "Spawn limits",
                    description =
                        "Decrease the spawn limit for water animals. We recommend using 5."
                });
            if (cfg.ContainsKey("spigot"))
            {
                var scfg = cfg["spigot"];
                var sws = scfg["world-settings"];
                foreach (var world in sws.ToObject<Dictionary<string, JObject>>())
                {
                    var ear = world.Value["entity-activation-range"];
                    if (ear != null)
                    {
                        if (ear["animals"].ToObject<int>() >= 32)
                            recommendationsList.Add(new TimingsRecommendations
                            {
                                title = $"world.{world.Key}.entity-activation-range.animals",
                                description = "Decrease this in [spigot.yml](http://bit.ly/spiconf). Recommended: 16."
                            });
                        if (ear["monsters"].ToObject<int>() >= 32)
                            recommendationsList.Add(new TimingsRecommendations
                            {
                                title = $"world.{world.Key}.entity-activation-range.monsters",
                                description = "Decrease this in [spigot.yml](http://bit.ly/spiconf). Recommended: 16."
                            });
                        if (ear["misc"].ToObject<int>() >= 16)
                            recommendationsList.Add(new TimingsRecommendations
                            {
                                title = $"world.{world.Key}.entity-activation-range.misc",
                                description =
                                    "Decrease this in [spigot.yml](http://bit.ly/spiconf). Recommended: 12."
                            });
                        if (ear["water"].ToObject<int>() >= 16)
                            recommendationsList.Add(new TimingsRecommendations
                            {
                                title = $"world.{world.Key}.entity-activation-range.water",
                                description =
                                    "Decrease this in [spigot.yml](http://bit.ly/spiconf). Recommended: 12."
                            });
                        if (ear["villagers"].ToObject<int>() >= 32)
                            recommendationsList.Add(new TimingsRecommendations
                            {
                                title = $"world.{world.Key}.entity-activation-range.villagers",
                                description =
                                    "Decrease this in [spigot.yml](http://bit.ly/spiconf). Recommended: 16."
                            });
                        if (ear["tick-inactive-villagers"].ToObject<bool>())
                            recommendationsList.Add(new TimingsRecommendations
                            {
                                title = $"world.{world.Key}.entity-activation-range.tick-inactivate-villagers",
                                description =
                                    "Disable this in [spigot.yml](http://bit.ly/spiconf)."
                            });
                        if (ear["wake-up-inactive"]["villagers-max-per-tick"].ToObject<int>() >= 1 &&
                            ear["wake-up-inactive"]["villagers-for"].ToObject<int>() >= 100)
                            recommendationsList.Add(new TimingsRecommendations
                            {
                                title = $"world.{world.Key}.entity-activation-range.wake-up-inactive.villagers-for",
                                description =
                                    "Decrease this in [spigot.yml](http://bit.ly/spiconf). Recommended: 20."
                            });
                        if (ear["wake-up-inactive"]["flying-monsters-max-per-tick"].ToObject<int>() >= 1 &&
                            ear["wake-up-inactive"]["flying-monsters-for"].ToObject<int>() >= 100)
                            recommendationsList.Add(new TimingsRecommendations
                            {
                                title =
                                    $"world.{world.Key}.entity-activation-range.wake-up-inactive.flying-monsters-for",
                                description =
                                    "Decrease this in [spigot.yml](http://bit.ly/spiconf). Recommended: 60."
                            });
                        if (ear["wake-up-inactive"]["animals-max-per-tick"].ToObject<int>() >= 1 &&
                            ear["wake-up-inactive"]["animals-for"].ToObject<int>() >= 100)
                            recommendationsList.Add(new TimingsRecommendations
                            {
                                title = $"world.{world.Key}.entity-activation-range.wake-up-inactive.animals-for",
                                description =
                                    "Decrease this in [spigot.yml](http://bit.ly/spiconf). Recommended: 40."
                            });
                        if (ear["wake-up-inactive"]["monsters-max-per-tick"].ToObject<int>() >= 1 &&
                            ear["wake-up-inactive"]["monsters-for"].ToObject<int>() >= 100)
                            recommendationsList.Add(new TimingsRecommendations
                            {
                                title = $"world.{world.Key}.entity-activation-range.wake-up-inactive.monsters_for",
                                description =
                                    "Decrease this in [spigot.yml](http://bit.ly/spiconf). Recommended: 60."
                            });
                        if (ear["wake-up-inactive"]["villagers-max-per-tick"].ToObject<int>() >= 4)
                            recommendationsList.Add(new TimingsRecommendations
                            {
                                title =
                                    $"world.{world.Key}.entity-activation-range.wake-up-inactive.villagers-max-per-tick",
                                description =
                                    "Decrease this in [spigot.yml](http://bit.ly/spiconf). Recommended: 1."
                            });
                        if (ear["wake-up-inactive"]["monsters-max-per-tick"].ToObject<int>() >= 8)
                            recommendationsList.Add(new TimingsRecommendations
                            {
                                title =
                                    $"world.{world.Key}.entity-activation-range.wake-up-inactive.monsters-max-per-tick",
                                description =
                                    "Decrease this in [spigot.yml](http://bit.ly/spiconf). Recommended: 4."
                            });
                        if (ear["wake-up-inactive"]["flying-monsters-max-per-tick"].ToObject<int>() >= 8)
                            recommendationsList.Add(new TimingsRecommendations
                            {
                                title =
                                    $"world.{world.Key}.entity-activation-range.wake-up-inactive.flying_monsters_max_per_tick",
                                description =
                                    "Decrease this in [spigot.yml](http://bit.ly/spiconf). Recommended: 1."
                            });
                        if (ear["wake-up-inactive"]["animals-max-per-tick"].ToObject<int>() >= 4)
                            recommendationsList.Add(new TimingsRecommendations
                            {
                                title =
                                    $"world.{world.Key}.entity-activation-range.wake-up-inactive.animals-max-per-tick",
                                description =
                                    "Decrease this in [spigot.yml](http://bit.ly/spiconf). Recommended: 2."
                            });
                    }

                    if (world.Value.ContainsKey("nerf-spawner-mobs") &&
                        world.Value["nerf-spawner-mobs"].ToObject<bool>())
                        recommendationsList.Add(new TimingsRecommendations
                        {
                            title = $"world.{world.Key}.nerf-spawner-mobs",
                            description =
                                "Enable this in [spigot.yml](http://bit.ly/spiconf)."
                        });
                    if (world.Value.ContainsKey("arrow-despawn-rate") &&
                        world.Value["arrow-despawn-rate"].ToObject<int>() >= 1200)
                        recommendationsList.Add(new TimingsRecommendations
                        {
                            title = $"world.{world.Key}.arrow-despawn-rate",
                            description =
                                "Decrease this in [spigot.yml](http://bit.ly/spiconf). Recommended: 300."
                        });
                    if (world.Value.ContainsKey("merge-radius") &&
                        world.Value["merge-radius"]["item"].ToObject<float>() <= 2.5)
                        recommendationsList.Add(new TimingsRecommendations
                        {
                            title = $"world.{world.Key}.merge-radius",
                            description =
                                "Increase this in [spigot.yml](http://bit.ly/spiconf). Recommended: 4.0."
                        });
                    if (world.Value.ContainsKey("merge-radius") &&
                        world.Value["merge-radius"]["exp"].ToObject<float>() <= 3.0)
                        recommendationsList.Add(new TimingsRecommendations
                        {
                            title = $"world.{world.Key}.",
                            description =
                                "Increase this in [spigot.yml](http://bit.ly/spiconf). Recommended: 6.0."
                        });
                    if (world.Value.ContainsKey("max-entity-collisions") &&
                        world.Value["max-entity-collisions"].ToObject<int>() >= 8)
                        recommendationsList.Add(new TimingsRecommendations
                        {
                            title = $"world.{world.Key}.max-entity-collisions",
                            description =
                                "Decrease this in [spigot.yml](http://bit.ly/spiconf). Recommended: 2."
                        });
                }

                if (cfg.ContainsKey("paper"))
                {
                }

                if (cfg.ContainsKey("purpur"))
                {
                    if (plugins.ContainsKey("TCPShield") &&
                        cfg["purpur"]["settings"]["use-alternative-keepalive"].ToObject<bool>())
                        recommendationsList.Add(new TimingsRecommendations
                        {
                            title = "TCPShield: alternate-keepalive",
                            description =
                                "TCPShield is incompatible with Purpur's alternate keepalive! Disable settings.use-alternate-keepalive in purpur.yml!"
                        });
                    else if (!cfg["purpur"]["settings"]["use-alternate-keepalive"].ToObject<bool>())
                        recommendationsList.Add(new TimingsRecommendations
                        {
                            title = "Alternate keepalive",
                            description = "It is recommended to enable purpur's keepalive."
                        });
                }
            }
        }

        return recommendationsList;
    }
}