using Bots.SugarcaneBot.DataStructures.Timings.timings.aikar.co.Recommendations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Bots.SugarcaneBot.DataStructures.Timings.timings.aikar.co;

public class AikarTimingsRoot
{
    [JsonProperty("id")] public string Id { get; set; }
    [JsonProperty("timingsMaster")] public AikarTimingsMaster TimingsMaster { get; set; }

    public List<TimingsRecommendations> GetRecommendations()
    {
        return AikarRecommendations.GetRecommendations(this);
    }
}

public class AikarTimingsMaster
{
    [JsonProperty("version")] public string Version { get; set; }
    [JsonProperty("maxplayers")] public int Maxplayers { get; set; }
    [JsonProperty("start")] public int Start { get; set; }
    [JsonProperty("end")] public int End { get; set; }
    [JsonProperty("sampletime")] public int Sampletime { get; set; }
    [JsonProperty("server")] public string Server { get; set; }
    [JsonProperty("motd")] public string Motd { get; set; }
    [JsonProperty("onlinemode")] public bool Onlinemode { get; set; }
    [JsonProperty("icon")] public string Icon { get; set; }

    [JsonProperty("system")] public AikarTimingsSystem System { get; set; }
    [JsonProperty("plugins")] public Dictionary<string, AikarPluginInfo> Plugins { get; set; }
    [JsonProperty("config")] public Dictionary<string, JObject> Configs { get; set; }
}

public class AikarTimingsSystem
{
    [JsonProperty("timingcost")] public int Timingcost { get; set; }
    [JsonProperty("name")] public string Name { get; set; }
    [JsonProperty("version")] public string Version { get; set; }
    [JsonProperty("jvmversion")] public string Jvmversion { get; set; }
    [JsonProperty("arch")] public string Arch { get; set; }
    [JsonProperty("maxmem")] public long Maxmem { get; set; }
    [JsonProperty("cpu")] public int Cpu { get; set; }
    [JsonProperty("runtime")] public int Runtime { get; set; }
    [JsonProperty("flags")] public string Flags { get; set; }
    [JsonProperty(":cls")] public int Cls { get; set; }
}

public class AikarPluginInfo
{
    [JsonProperty("name")] public string Name { get; set; }
    [JsonProperty("version")] public string Version { get; set; }
    [JsonProperty("description")] public string Description { get; set; }
    [JsonProperty("website")] public object Website { get; set; }
    [JsonProperty("authors")] public string Authors { get; set; }
    [JsonProperty(":cls")] public int Cls { get; set; }
}