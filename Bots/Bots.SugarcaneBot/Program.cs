using System.Net;
using BotCore.DataModel;
using BotCore.DbExtras;
using BotCore.Util.DbExtensions;
using Bots.SugarcaneBot;
using Bots.SugarcaneBot.DataStructures.Timings.timings.aikar.co;
using Newtonsoft.Json;

var aikartest = JsonConvert.DeserializeObject<AikarTimingsRoot>(File.ReadAllText("aikar.json"));
Console.WriteLine(JsonConvert.SerializeObject(aikartest.TimingsMaster.Configs, new JsonSerializerSettings
{
    Formatting = Formatting.Indented,
    ContractResolver = new LongNameContractResolver()
}));
aikartest.GetRecommendations();
//Environment.Exit(0);

if (!Directory.Exists("timings")) Directory.CreateDirectory("timings");
foreach (var url in File.ReadAllLines("timings.txt"))
{
    var id = url.Split("id=")[1].Split("#")[0];
    if (!File.Exists($"timings/{id}.json"))
    {
        Console.WriteLine("Downloading: " + url);
        new WebClient().DownloadFile(url.Replace("/?", "/data.php?"), $"timings/{id}.json");
    }

    var timing = JsonConvert.DeserializeObject<AikarTimingsRoot>(File.ReadAllText($"timings/{id}.json"));

    Console.WriteLine($"ID: {timing.Id}");
    Console.WriteLine($"Version: {timing.TimingsMaster.Version}");
    foreach (var rec in timing.GetRecommendations()) Console.WriteLine(rec.title + ": " + rec.description);
}

Environment.Exit(0);

var db = Db.GetPostgres();
var bot = db.GetBot("sugarcanebot");
var bi = new BotImplementation(db, bot);

// fetch full list of posted timings in bot-spam
/*
var c = bi.DiscordClient.GetChannelAsync(856305080910872576).Result;
ulong minId = 964114128639766528;
bool running = true;

var tf = File.OpenWrite("timings.txt");
var tw = new StreamWriter(tf);
while (running)
{
    Console.WriteLine("Fetching 100 msgs!");
    var msgs = c.GetMessagesBeforeAsync(minId).Result;
    foreach (var msg in msgs.Where(x => x.Content.Contains("timings.aikar.co")))
    {
        tw.WriteLine(msg.Content);
    }

    if (msgs.Count < 1 || minId == msgs.Min(x => x.Id)) running = false;
    else minId = msgs.Min(x => x.Id);
    tw.Flush();
    tf.Flush();
}
Console.WriteLine("done!");

tw.Close();
File.WriteAllLines("timings.txt", File.ReadAllLines("timings.txt").Distinct());
Environment.Exit(0);
*/

Thread.Sleep(int.MaxValue);