﻿using BotCore.DataModel;
using BotCore.DbExtras;
using BotCore.Util.DbExtensions;
using Bots.FosscordBot.Commands;

var db = Db.GetPostgres();
var bot = db.GetBot("fosscordbot");
var bi = new BotImplementation(db, bot);
bi.CommandManager.RegisterCommandsInType(typeof(Github));
bi.CommandManager.RegisterCommandsInType(typeof(ImportToFosscord));

Thread.Sleep(int.MaxValue);