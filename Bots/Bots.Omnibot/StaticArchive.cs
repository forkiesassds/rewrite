using System.Net;
using BotCore.DataModel;
using Bots.Omnibot.Commands;

namespace Bots.Omnibot;

public class StaticArchive
{
    public static string basePage =
        new WebClient().DownloadString("https://omniarchive.thearcanebrony.net/static/pagetemplate.html");

    public static string whiteBox =
        new WebClient().DownloadString("https://omniarchive.thearcanebrony.net/static/whiteboxtemplate.html");

    public static string GetWhiteBox(string title, string date, string description, string downloadtext = "",
        string url = "", string image_url = "https://omniarchive.net/files/missing.png")
    {
        return whiteBox
            .Replace("$TITLE", title)
            .Replace("$DATE", date)
            .Replace("$DESCRIPTION", description.Length < 1 ? "No description." : description)
            .Replace("$DOWNLOAD", downloadtext)
            .Replace("$URL", url)
            .Replace("$IMG", image_url);
    }

    public static string GetBasePage(string title, string header, string content)
    {
        return basePage
            .Replace("$TITLE", title)
            .Replace("$HEADER", header)
            .Replace("<!--Content-->", content);
    }

    public static string GetDetailsPage(DownloadInfo di, GlobalUser? user = null)
    {
        // user ??= bot.GetNewDbBot().bot.GetServer(361634042317111296).GetUser(di.UserId);
        if (user == null) throw new NullReferenceException("Expected user");
        return GetBasePage(di.MessageId.ToString(), $"Detailed info for {di.MessageId}",
            GetWhiteBox($"<a href='{di.JumpLink}'>{user.Username}</a>", di.Timestamp.ToString(), di.MessageContent));
    }

    public static string GetIndexEntryForDownloadInfo(DownloadInfo di, GlobalUser? user = null)
    {
        // user ??= bot.GetNewDbBot().bot.GetServer(361634042317111296).GetUser(di.UserId);
        if (user == null) throw new NullReferenceException("Expected user");
        return GetWhiteBox(user.Username, di.Timestamp.ToString(), di.MessageContent,
            "More info...", di.MessageId.ToString(), user.AvatarUrl);
    }
}