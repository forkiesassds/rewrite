﻿// See https://aka.ms/new-console-template for more information

using BotCore.Classes;
using BotCore.DataModel;
using BotCore.DbExtras;
using BotCore.Util.DbExtensions;
using Bots.Omnibot.Commands;
using Index = Bots.Omnibot.Commands.Index;
using Monitor = BotCore.Monitoring.Monitor;


var db = Db.GetPostgres();
var bot = db.GetBot("omnibot");

Index.index.ParseIndex();

var bi = new BotImplementation(db, bot);

Monitor.StartMonitoring(bi);
bi.CommandManager.RegisterCommandsInType(typeof(Archival));
bi.CommandManager.RegisterCommandsInType(typeof(Debug));
bi.CommandManager.RegisterCommandsInType(typeof(Bots.Omnibot.Commands.Index));
bi.CommandManager.RegisterCommandsInType(typeof(Meta));
bi.EventLog.AddEndpoint(new DiscordEndpoint(bi.DiscordClient.GetChannelAsync(971145097980833822).Result));
bi.SysLog.AddEndpoint(new DiscordEndpoint(bi.DiscordClient.GetChannelAsync(971145084009611324).Result));
bi.AlertLog.AddEndpoint(new DiscordEndpoint(bi.DiscordClient.GetChannelAsync(971145017261453433).Result));

Thread.Sleep(int.MaxValue);