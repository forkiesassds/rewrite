using System.Diagnostics;
using ArcaneLibs;
using BotCore.Commands;
using BotCore.DataModel;
using BotCore.DbExtras;
using DSharpPlus.Entities;
using Newtonsoft.Json;

namespace Bots.Omnibot.Commands;

public class Archival
{
    private static Random rnd = new();

    //misc downloads archive
    public static Command ArchiveDLCommand(BotImplementation bot)
    {
        return new Command
        {
            Name = "archivedl",
            Category = "Owner",
            Description = "Archive <#369990015096455168>",
            action = async (me, ce) =>
            {
                var sw = Stopwatch.StartNew();
                var lastUpdate = new DateTime();
                Directory.CreateDirectory("downloads");
                var status_message =
                    await me.Channel.SendMessageAsync("Started archiving channel: <#369990015096455168>");
                var downloadQueue = new DownloadQueue();
                var channel = await bot.DiscordClient.GetChannelAsync(369990015096455168);
                ulong lastMessage = 369990184630091776;
                var i = 0;
                var downloaded = 0;
                var redownloaded = 0;
                var verified = 0;
                while (lastMessage < channel.LastMessageId)
                {
                    IReadOnlyList<DiscordMessage> msgs = await channel.GetMessagesAfterAsync(lastMessage);
                    foreach (var msg in msgs)
                    {
                        i++;
                        Directory.CreateDirectory("downloads/" + msg.Id);
                        var dla = new DownloadInfo
                        {
                            MessageContent = msg.Content,
                            MessageId = msg.Id,
                            UserId = msg.Author.Id,
                            JumpLink = msg.JumpLink.ToString(),
                            Timestamp = msg.Timestamp
                        };
                        if (msg.Attachments.Count > 0)
                        {
                            Directory.CreateDirectory($"downloads/{msg.Id}/attachments");
                            foreach (var da in msg.Attachments)
                            {
                                dla.Attachments.Add(da.FileName);
                                if (!File.Exists($"downloads/{msg.Id}/attachments/{da.FileName}"))
                                {
                                    downloadQueue.Add(new ArcaneLibs.DownloadInfo
                                    {
                                        Size = da.FileSize,
                                        Url = da.Url,
                                        FileName = da.FileName,
                                        TargetFile = $"downloads/{msg.Id}/attachments/{da.FileName}"
                                    });
                                    downloaded++;
                                }
                                else
                                {
                                    if (da.FileSize != new FileInfo($"downloads/{msg.Id}/attachments/{da.FileName}")
                                            .Length)
                                    {
                                        Console.WriteLine(
                                            $"Filesize mismatch for downloads/{msg.Id}/attachments/{da.FileName}, redownloading!");
                                        downloadQueue.Add(new ArcaneLibs.DownloadInfo
                                        {
                                            Size = da.FileSize,
                                            Url = da.Url,
                                            FileName = da.FileName,
                                            TargetFile = $"downloads/{msg.Id}/attachments/{da.FileName}"
                                        });
                                        redownloaded++;
                                    }
                                    else
                                    {
                                        Console.WriteLine(
                                            $"File size matched for downloads/{msg.Id}/attachments/{da.FileName}!");
                                        verified++;
                                    }
                                }
                            }

                            Util.WriteAllTextIfDifferent($"downloads/{msg.Id}/download.json",
                                JsonConvert.SerializeObject(dla, Formatting.Indented));
                        }
                    }

                    if ((DateTime.Now - lastUpdate).TotalSeconds >= 1)
                    {
                        status_message.ModifyAsync(
                            $"Processed {i} messages in {sw.Elapsed}...\nDownload queue: {downloadQueue.queue.Count} ({Util.BytesToString(downloadQueue.queue.Sum(x => x.Size))})");
                        lastUpdate = DateTime.Now;
                    }

                    lastMessage = msgs.Count > 0 ? msgs.Max(x => x.Id) : lastMessage;
                }

                downloadQueue.UpdatingQueue = false;
                downloadQueue.ShouldQueueMore = q => q.running.Count < 8;
                Console.WriteLine("Starting download:");

                var progressBarMessage = await me.Channel.SendMessageAsync("Starting download!");
                var x = downloadQueue.Run();
                Console.WriteLine("yes");
                while (!downloadQueue.AllDownloadsFinished)
                    if ((DateTime.Now - lastUpdate).TotalSeconds >= 1)
                    {
                        var text =
                            $"Download Progress: {downloadQueue.queue.Count(x => x.Progress == 1)}/{downloadQueue.queue.Count} ({downloadQueue.queue.Average(x => x.Progress) * 100:F2}%)\n```{downloadQueue.GetProgressBars()}```";
                        progressBarMessage = await progressBarMessage.ModifyAsync(text);

                        lastUpdate = DateTime.Now;
                    }
                // Thread.Sleep(2000);

                Console.WriteLine("Downloads finished?");
                await x;
                Console.WriteLine("Downloads finished!");

                sw.Stop();
                status_message = await status_message.ModifyAsync(
                    string.Join("\n", status_message.Content.Split("\n").TakeLast(4)) +
                    $"\nFinished processing messages! Processed {i} messages in {sw.Elapsed}..." +
                    $"\nDownloaded {downloaded} new files, redownloaded {redownloaded} files..." +
                    $"\n{verified} out of {downloaded + redownloaded + verified} files already downloaded passed verification!");
                RebuildPages(bot, ref status_message);
                return null;
            }
        };
    }

    //Reprocess page
    public static Command RebuildPagesCommand(BotImplementation bot)
    {
        return new Command
        {
            Name = "rebuildpages",
            Category = "Owner",
            Description = "Rebuild all pages for <#369990015096455168>",
            action = async (me, ce) =>
            {
                var status_message =
                    await me.Channel.SendMessageAsync("Started rebuilding static pages!");
                RebuildPages(bot, ref status_message);
                return null;
            }
        };
    }

    public static Command ScrapeCommand(BotImplementation bot)
    {
        return new Command
        {
            Name = "scrape",
            Category = "Owner",
            Description = "Scrape web page",
            action = async (me, ce) =>
            {
                var msg = await me.Channel.SendMessageAsync("Starting download...");
                var d = DateTime.Now.ToBinary();
                var dirname = "scrape/" + Math.Abs(d);
                Directory.CreateDirectory(dirname);
                Util.RunCommandInDirSync(dirname, "wget", $"-np -r {ce.Args[0]}");
                Util.RunCommandInDirSync(dirname, "7z", $"-sdel -r -v8m a scrape{d}.7z");
                var files = Directory.GetFiles(dirname).OrderBy(x => x);
                foreach (var file in files)
                {
                    var fs = File.Open(file, FileMode.Open);
                    await me.Channel.SendMessageAsync(x => x.WithFile(new FileInfo(file).Name, fs));
                    fs.Close();
                }

                return null;
            },
            CanRun = (me, ce) => Task.FromResult(me.Channel.Id == 511326509059801088)
        };
    }

    public static void RebuildPages(BotImplementation bot, ref DiscordMessage statusMessage)
    {
        var sw = Stopwatch.StartNew();
        var tn = "BuildPages";
        // Stopwatch t = bot.Timing.StartTiming(tn);
        Directory.CreateDirectory("downloads");
        Dictionary<ulong, string> _index = new();
        Dictionary<ulong, string> _details = new();
        var db = Db.GetNewPostgres();
        // Server server = bot.db.Servers.First(x=>x.DiscordServerId == 361634042317111296.ToString());
        // t = bot.Timing.GotoNext(t, tn + ".ProcessPages");
        foreach (var dir in Directory.GetDirectories("downloads"))
        {
            // Console.WriteLine(dir);
            if (!File.Exists(dir + "/download.json"))
            {
                Directory.Delete(dir, true);
                continue;
            }

            var di =
                JsonConvert.DeserializeObject<DownloadInfo>(File.ReadAllTextAsync($"{dir}/download.json").Result);
            var user = db.GlobalUsers.FirstOrDefault(x => x.DiscordUserId == di.UserId.ToString());
            if (user == null)
            {
                Console.WriteLine("No such user: " + di.UserId);
                continue;
            }
            // if (user.GlobalUser.Username == "")
            // {
            //     try
            //     {
            //         var usr = bot.DiscordClient.GetUserAsync(ulong.Parse(user.GlobalUser.DiscordUserId)).Result;
            //         user.GlobalUser.Username = usr.Username;
            //         user.GlobalUser.AvatarUrl = usr.GetAvatarUrl(ImageFormat.Png, 2048);
            //     }
            //     catch
            //     {
            //         user.GlobalUser.Username = "Unknown user";
            //         user.GlobalUser.AvatarUrl = $"https://cdn.discordapp.com/embed/avatars/{rnd.Next(6)}.png";
            //     }
            // }

            _details.Add(di.MessageId, StaticArchive.GetDetailsPage(di, user));
            _index.Add(di.MessageId, StaticArchive.GetIndexEntryForDownloadInfo(di, user));
        }

        // dbBot.db.SaveChanges();

        // t = bot.Timing.GotoNext(t, tn + ".WriteDetails");
        Console.WriteLine("Writing detail pages...");
        foreach (var x in _details) File.WriteAllText($"downloads/{x.Key}/index.html", x.Value);

        // t = bot.Timing.GotoNext(t, tn + ".WriteIndex");
        Console.WriteLine("Writing main index...");
        File.WriteAllText("downloads/index.html",
            StaticArchive.GetBasePage("Misc downloads",
                $"Omniarchive #misc-downloads archive, archived at {DateTime.Now}",
                string.Join("\n", _index.OrderBy(x => x.Key).Select(x => x.Value))));
        db.Dispose();
        sw.Stop();
        // bot.Timing.StopTiming(t);
        statusMessage = statusMessage.ModifyAsync(
                string.Join("\n", statusMessage.Content.Split("\n").TakeLast(5)) +
                $"\nFinished rebuilding static pages! Processed {Directory.GetDirectories("downloads").Length} directories in {sw.Elapsed}...")
            .Result;
    }
}

public class DownloadInfo
{
    public string MessageContent { get; set; }
    public ulong MessageId { get; set; }
    public ulong UserId { get; set; }
    public string JumpLink { get; set; }
    public DateTimeOffset Timestamp { get; set; }
    public List<string> Attachments { get; set; } = new();
}