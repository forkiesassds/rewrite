using System.Net;
using System.Security.Cryptography;
using BotCore.Commands;
using BotCore.DbExtras;
using OmniarchiveIndexParser;

namespace Bots.Omnibot.Commands;

public class Index
{
    public static IndexHandler index = new();
    //lookup command, looks up a minecraft client/server by version
    public static Command LookupCommand(BotImplementation bot)
    {
        return new Command
        {
            Name = "lookup",
            Category = "Minecraft",
            Description = "Show whether a client or server has been found",
            action = async (me, ce) =>
            {
                if (ce.Args.Count > 3)
                {
                    ce.Args[2] = string.Join(" ", ce.Args.Skip(2));
                    ce.Args.RemoveRange(3, ce.Args.Count - 2);
                }

                var index = OmniarchiveIndexParser.Index.Instance;
                Console.WriteLine(ce.Args.Count);
                var validargs = true;

                if (ce.Args.Count != 3)
                {
                    validargs = false;
                }
                else
                {
                    switch (ce.Args[0].ToLower())
                    {
                        case "j":
                        case "java":
                            ce.Args[0] = "java";
                            break;
                        /*case "b":
                    case "bedrock":
                        ce.Args[0] = "bedrock";
                        break;*/
                        default:
                            validargs = false;
                            break;
                    }

                    switch (ce.Args[1].ToLower())
                    {
                        case "c":
                        case "client":
                        case "clients":
                            ce.Args[1] = "clients";
                            break;
                        case "s":
                        case "server":
                        case "servers":
                            ce.Args[1] = "servers";
                            break;
                        default:
                            validargs = false;
                            break;
                    }

                    try
                    {
                        Console.WriteLine(string.Join(", ", ce.Args));
                        var results = 0;
                        foreach (var ver in index.VersionList[ce.Args[0] + ce.Args[1]])
                            if (ver.Id == ce.Args[2] || ver.Version == ce.Args[2])
                                //await me.Channel.SendMessageAsync($"Found possible match: ```json\n{JsonConvert.SerializeObject(ver, Formatting.Indented)}```"/*, embed: embed.Build()*/);
                                if (results++ < 3)
                                    await me.Channel.SendMessageAsync(ver.GetVersionEmbed());

                        if (results == 0)
                            await me.Channel.SendMessageAsync(
                                "No results found, if this is in error according to the index (visible at <https://derpy.me/omnidex>), report this to The Arcane Brony!\nIndex last updated: " +
                                index.LastUpdated);
                        else
                            await me.Channel.SendMessageAsync(
                                $"Found {results} results! If anything shows up that didn't even match your query, report this to The Arcane Brony!\nIndex last updated: {index.LastUpdated}");
                    }
                    catch (KeyNotFoundException)
                    {
                        await me.Channel.SendMessageAsync(
                            "Invalid type args have been passed, proper error coming some day...");
                    }
                    catch (Exception err)
                    {
                        await me.Channel.SendMessageAsync(
                            $"[<@84022289024159744>] An error ocurred ```csharp\n{err}```");
                    }
                }

                if (!validargs)
                    await me.Channel.SendMessageAsync(
                        $"Syntax: `{me.Server.Prefix}lookup <edition> <type> <version>`\nEditions: `Java`, `J`\nTypes: `Client`, `Server`, `Clients`, `Servers`, `C`, `S`\n*These are not case sensitive*\n\nGiven args: {string.Join(", ", ce.Args)}");

                return null;
            }
        };
    }

    //pvn command, looks up clients and servers with a given pvn
    public static Command PvnCommand(BotImplementation bot)
    {
        return new Command
        {
            Name = "pvn",
            Category = "Minecraft",
            Description = "List of Minecraft servers and clients that use this protocol version number",
            action = async (me, ce) =>
            {
                try
                {
                    var index = OmniarchiveIndexParser.Index.Instance;

                    List<string> clients = new(), servers = new();
                    foreach (var ver in index.VersionList["javaclients"])
                        if (ver.Pvn == ce.Args[0])
                            clients.Add(ver.Id);

                    foreach (var ver in index.VersionList["javaservers"])
                        if (ver.Pvn == ce.Args[0])
                            servers.Add(ver.Id);

                    await me.Channel.SendMessageAsync(
                        $"Clients: {string.Join(", ", clients)}\nServers: {string.Join(", ", servers)}\n{index.FileDate}");
                }
                catch (KeyNotFoundException err)
                {
                    await me.Channel.SendMessageAsync($"[<@84022289024159744>] Key not found ```cs\n{err}```");
                }
                catch (Exception err)
                {
                    await me.Channel.SendMessageAsync(
                        $"[<@84022289024159744>] An error has ocurred ```csharp\n{err}```");
                }

                return null;
            }
        };
    }

    //identify command, identifies a jar file (attachment) by hash
    public static Command IdentifyCommand(BotImplementation bot)
    {
        return new Command
        {
            Name = "identify",
            Category = "Minecraft",
            Description = "Identify a Minecraft version based on hash(es) or attachment(s) (Latter is DM only)",
            action = async (me, ce) =>
            {
                var index = OmniarchiveIndexParser.Index.Instance;
                var found = false;
                if (me.Message.Attachments.Count != 0 && me.Channel.Guild == null)
                    using (var md5 = MD5.Create())
                    {
                        using (WebClient wc = new())
                        {
                            foreach (var att in me.Message.Attachments)
                            {
                                var hashed = md5.ComputeHash(wc.DownloadData(att.Url));
                                var hash = BitConverter.ToString(hashed).Replace("-", "");

                                await me.Channel.SendMessageAsync($"The MD5 for *{att.FileName}* is `{hash}`");
                                foreach (List<McInfo> info in index.VersionList.Values)
                                foreach (var mcinfo in info)
                                    if (mcinfo.Md5 == hash)
                                    {
                                        await me.Channel.SendMessageAsync(index.FileDate,
                                            mcinfo.GetVersionEmbed());
                                        found = true;
                                    }
                            }
                        }
                    }

                foreach (var arg in ce.Args)
                foreach (List<McInfo> info in index.VersionList.Values)
                foreach (var mcinfo in info)
                    if (mcinfo.Sha256 == arg || mcinfo.Md5 == arg)
                    {
                        await me.Channel.SendMessageAsync(index.FileDate, mcinfo.GetVersionEmbed());
                        found = true;
                    }

                if (!found)
                    await me.Channel.SendMessageAsync("No version(s) found matching the criteria!\n" + index.FileDate);
                return null;
            }
        };
    }
}