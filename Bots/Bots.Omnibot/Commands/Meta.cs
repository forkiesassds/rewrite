using BotCore.Commands;
using BotCore.DbExtras;

namespace Bots.Omnibot.Commands;

public class Meta
{
    //index command, sends a link to the omniarchive index
    public static Command IndexCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "index",
            Category = "Minecraft",
            Description = "Get a link to the Omniarchive index",
            action = async (me, ce) =>
            {
                // if (ce.E.Guild.Id != 361634042317111296)
                await me.Channel.SendMessageAsync("View the full index at <https://derpy.me/omnidex>");
                return null;
            }
        };
    }

    //archive command, posts archive.org links
    public static Command ArchiveCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "archive",
            Category = "Minecraft",
            Description = "Get the archive.org links",
            action = async (me, ce) =>
            {
                // if (ce.E.Guild.Id != 361634042317111296)
                await me.Channel.SendMessageAsync(
                    "Archive.org links:\n\nPre-Classic: <https://archive.org/details/Minecraft-JE-Pre-Classic>\nClassic: <https://archive.org/details/Minecraft-JE-Classic>\nIndev: <https://archive.org/details/Minecraft-JE-Indev>\nInfdev: <https://archive.org/details/Minecraft-JE-Infdev>\nAlpha: <https://archive.org/details/Minecraft-JE-Alpha>\nBeta: <https://archive.org/details/Minecraft-JE-Beta>");
                return null;
            }
        };
    }
}