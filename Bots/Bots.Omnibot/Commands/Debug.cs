using BotCore.Commands;
using BotCore.DbExtras;
using Newtonsoft.Json;

namespace Bots.Omnibot.Commands;

public class Debug
{
    //debugindex, uploads the index as a json to see how it was interpreted
    public static Command DebugIndexCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "debugindex",
            Category = "Developer",
            Description = "Check how the bot read the index",
            action = async (me, ce) =>
            {
                /*await File.WriteAllTextAsync("index.json",
                    JsonConvert.SerializeObject(OmniarchiveIndexParser.Index.Instance, Formatting.Indented));*/
                var fs = File.Open("index-new.json", FileMode.Open);
                await me.Channel.SendMessageAsync(x =>
                    x.WithFile("index.json", fs)
                        .WithContent(
                            $"Here's how I interpreted the index!\nData was last updated on: {OmniarchiveIndexParser.Index.Instance.FileDate}"));
                fs.Close();
                return null;
            }
        };
    }
}