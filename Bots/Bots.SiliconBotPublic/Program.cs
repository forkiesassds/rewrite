using BotCore.DataModel;
using BotCore.DbExtras;
using BotCore.Util.DbExtensions;

var db = Db.GetPostgres();
var bot = db.GetBot("siliconbotpublic");
var bi = new BotImplementation(db, bot);

Thread.Sleep(int.MaxValue);