function PrepareModal() {
    $("body").append(`<div id='modal-bg'></div>`);
    $("#modal-bg").click(CloseModal);
}

function CloseModal(a) {
    if (a.target.id === "modal-bg") {
        $(".modal").animate({
            opacity: 0,
            width: 0,
            height: 0
        }, 500, function () {
            $(".modal").remove();
        });
        $("#modal-bg").animate({
            opacity: 0
        }, 500, function () {
            $("#modal-bg").remove();
        });
    }
}

function OpenModal(id) {
    PrepareModal();
    $("#modal-bg").append(`<div id='userpanel' class='modal'><div class="loader">Loading...</div></div>`);
}

function checkVisible(elm, evalType) {
    evalType = evalType || "visible";

    var vpH = $(window).height(), // Viewport Height
        st = $(window).scrollTop(), // Scroll Top
        y = $(elm).offset().top,
        elementHeight = $(elm).height();

    if (evalType === "visible") return ((y < (vpH + st)) && (y > (st - elementHeight)));
    if (evalType === "above") return ((y < (vpH + st)));
}

var loading = 0;

function loadImage(elem, url) {
    if (loading < 16) {
        if (checkVisible(elem, "above")) {
            loading++;
            elem.href.baseVal = url;
            elem.href.animVal = url;
            elem.onload = function () {
                loading--;
                // console.log(elem);
                // console.log("Loaded: " + elem);
            }
            elem.onerror = function (error) {
                loading--;
                console.log(error);
                fetch(new Request(elem.href.baseVal))
                    .then(resp => {
                        if (resp.status == 404) {
                            console.log(elem.accessKey);
                            // elem.href.baseVal = elem.href.animVal = "/api/avatar?id=" + elem.accessKey + "&invalid=true";
                        }
                    });
            }
        } else {
            setTimeout(() => loadImage(elem, url), 250);
        }
    } else {
        setTimeout(() => loadImage(elem, url), 500);
    }
}

//
// window.addEventListener("error", function (e) {
//     console.log(e);
//     
// }, true);

var botid = 0;
var serverid = 0;

function addUsers(list) {
    list.sort((a, b) => {
        if (a.xpLevel === b.xpLevel) return a.xp < b.xp ? 1 : -1;
        return a.xpLevel < b.xpLevel ? 1 : -1;
    });
    // console.log(list);
    var i = 0;
    list.forEach((a, b) => {
        if (!a.avatarUrl || a.avatarUrl === "") a.avatarUrl = "https://cdn.discordapp.com/embed/avatars/0.png?size=512";
        $("#content").append(`<div class="clickable card" onclick="OpenModal('${a.discordUserId}')">
		<a class="ranking">${++i}</a>
		<svg class="progress-ring icon" width="64" height="64" loading="lazy">
    <clipPath id="clipCircle" clipPathUnits="objectBoundingBox">
        <circle r=".5" cx=".5" cy=".5" />
    </clipPath>
    <circle class="progress-ring__circle" stroke="white" stroke-width="4" fill="transparent" r="28" cx="50%" cy="50%"/>
    <image clip-path="url(#clipCircle)" height="56" width="56" x="50%" y="50%" transform="translate(-28,-28)" data-yes="${a.discordUserId}"/>
</svg>
<!--		<img class='icon' loading='lazy' async accessKey='${a.discordUserId}' src='${a.avatarUrl}'>-->
		<a class="large">${a.username}<a class="extrasmall"></a></a><br>
		<a class="large">${a.xpLevel}</a>
		<a class="small extra">(${a.xp}/${a.xpGoal} XP)</a><br>
		<div class='pb cardfooter'>
			<span class="cardfooter" style='width: ${a.xpProgress * 100}%;'></span>
		</div>
	</div>`);
        loadImage($("#content").children().last()[0].querySelector('svg').querySelector('image'), a.avatarUrl);

    });
}

function pageLoaded() {
    fetch(new Request("/api/thisbot"))
        .then(response => response.json())
        .then(bot => {
            console.log(bot);
            botid = bot.botId;
            $("#footer").append(`</div><img src='https://img.shields.io/badge/BotCore-${bot.coreVer}-green'>`);
            // $("center").append(`<h1>Leaderboards for ${bot.name}</h1>`)
            serverid = document.location.pathname.split("/")[2];
            fetch(new Request(`/api/server?bid=${botid}&id=${serverid}`))
                .then(response => response.json())
                .then(server => {
                    $("#content").append(`<h1>Leaderboards for ${server.name}</h1>`);
                    fetch(new Request(`/api/users?bid=${botid}&dsid=${serverid}`))
                        .then(response => response.json())
                        .then(list => {
                            addUsers(list);
                        });
                });
        });
}

window.onload = pageLoaded;