using BotCore.DataModel;
using BotCore.Web.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace BotCore.Web.Controllers.Frontend;

[Controller]
[Route("/")]
public class FrontendController : Controller
{
    private readonly Db _db;

    public FrontendController(Db db)
    {
        _db = db;
    }

    [HttpGet]
    public object Home()
    {
        Console.WriteLine(Request.Host);
        var bot = Resolvers.GetBotByHost(_db, Request.Host.Host);
        return Resolvers.ReturnFileWithVars("Resources/Pages/index.html", _db, bot);
    }
}