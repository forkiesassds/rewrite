using ArcaneLibs;

namespace BotCore.Web;

public class Config : SaveableObject<Config>
{
    public string SentryEnvironment { get; } = "";
}