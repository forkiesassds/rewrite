//sql command
else if (cmd == "sql" && m.Author.Id == 84022289024159744) {
    await ch.SendMessageAsync($"Evaluating query: {cargstr}");
    Stopwatch sw = Stopwatch.StartNew();
    try {
        var conn = (NpgsqlConnection) db.Database.GetDbConnection();
        conn.Open();
        var qry = new NpgsqlCommand(cargstr, conn).ExecuteReader();
        string msg = "", tmsg = "";
        qry.Read();
        for (int i = 0; i < qry.FieldCount; i++) {
            msg += qry.GetName(i) + (i != qry.FieldCount - 1 ? "," : "\n");
        }

        tmsg = msg;

        await ch.SendMessageAsync($"```\n{msg}```");
        msg = "";
        for (int i = 0; i < qry.FieldCount; i++) {
            msg += qry[i] + (i != qry.FieldCount - 1 ? "," : "\n");
            tmsg += qry[i] + (i != qry.FieldCount - 1 ? "," : "\n");
        }

        while (qry.Read()) {
            for (int i = 0; i < qry.FieldCount; i++) {
                msg += qry[i] + (i != qry.FieldCount - 1 ? "," : "\n");
                tmsg += qry[i] + (i != qry.FieldCount - 1 ? "," : "\n");
            }
            if (msg.Length >= 1992) {
                var _msg = msg.Split("\n");
                var __msg = string.Join("\n", _msg.SkipLast(1));
                msg = _msg.Last();
                await ch.SendMessageAsync("```\n" + string.Join("\n", __msg) + "```\n");
            }
        }
        qry.Close();
        await ch.SendMessageAsync("```\n" + msg + "```\n");
        await ch.SendMessageAsync($"Execution finished! {qry.Rows} rows in {sw.Elapsed}.");
        conn.Close();
    }
    catch (Exception ex) {
        await ch.SendMessageAsync("Exception occurred: ```cs\n" + ex + "```\n");
        throw;
    }
    sw.Stop();
}

//exec sql
async void execsql(DiscordChannel ch, string cargstr, DatabaseFacade db) {
    await ch.SendMessageAsync($"Evaluating query: {cargstr}");
    Stopwatch sw = Stopwatch.StartNew();
    NpgsqlDataReader qry = null;
    NpgsqlConnection conn = null;
    try {
        conn = (NpgsqlConnection)db.GetDbConnection();
        conn.Open();
        
        qry = new NpgsqlCommand(cargstr, conn).ExecuteReader();
        string msg = "", tmsg = "";
        qry.Read();
        for (int i = 0; i < qry.FieldCount; i++) {
            msg += qry.GetName(i) + (i != qry.FieldCount - 1 ? "," : "\n");
        }

        tmsg = msg;

        await ch.SendMessageAsync($"```\n{msg}```");
        msg = "";
        for (int i = 0; i < qry.FieldCount; i++) {
            msg += qry[i] + (i != qry.FieldCount - 1 ? "," : "\n");
            tmsg += qry[i] + (i != qry.FieldCount - 1 ? "," : "\n");
        }

        while (qry.Read()) {
            for (int i = 0; i < qry.FieldCount; i++) {
                msg += qry[i] + (i != qry.FieldCount - 1 ? "," : "\n");
                tmsg += qry[i] + (i != qry.FieldCount - 1 ? "," : "\n");
            }

            if (msg.Length >= 1992) {
                var _msg = msg.Split("\n");
                var __msg = string.Join("\n", _msg.SkipLast(1));
                msg = _msg.Last();
                await ch.SendMessageAsync("```\n" + string.Join("\n", __msg) + "```\n");
            }
        }

        qry.Close();
        await ch.SendMessageAsync("```\n" + msg + "```\n");
        await ch.SendMessageAsync($"Execution finished! {qry.Rows} rows in {sw.Elapsed}.");
        conn.Close();
    }
    catch (Exception ex) {
        qry?.Close();
        conn?.Close();
        await ch.SendMessageAsync("Exception occurred: ```cs\n" + ex + "```\n");
        throw;
    }

    sw.Stop();
}