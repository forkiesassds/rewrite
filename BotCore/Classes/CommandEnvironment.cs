using BotCore.DbExtras;
using Sentry;

namespace BotCore.Commands;

public struct CommandEnvironment
{
    public Command Command;
    public CommandStatus CommandStatus;
    public List<string> Args { get; set; }
    public BotImplementation BotImplementation { get; set; }
    public ISpan Span { get; set; }
}