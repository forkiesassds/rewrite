using BotCore.DataModel;
using BotCore.DbExtras;
using DSharpPlus.Entities;

namespace BotCore.Classes;

public struct MessageEnvironment
{
    public BotImplementation Bot;
    public DiscordMessage Message;
    public DiscordChannel Channel;
    public GlobalUser User;
    public GlobalUser MentionedUser;
    public DServerUser DSUser;
    public DServerUser MentionedDSUser;
    public Server Server;
    public Db Db;
}