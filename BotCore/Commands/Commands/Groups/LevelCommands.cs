﻿using BotCore.DbExtras;

namespace BotCore.Commands.Commands.Groups;

internal class LevelCommands
{
    //profile command, displays level, xp, xp goal and premium status
    public static Command ProfileCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "profile",
            Category = "Leveling",
            Description = "Display user profile",
            action = async (me, ce) =>
            {
                //get user
                var user = me.Message.MentionedUsers.Count >= 1
                    ? me.Message.MentionedUsers[0]
                    : me.Message.Author;
                await me.Channel.SendMessageAsync(
                    $"Profile for ***{me.Channel.Guild.Members[user.Id]}***:\nLevel: {me.MentionedDSUser.XpLevel}\nXP: {me.MentionedDSUser.Xp} ({me.MentionedDSUser.XpMultiplier}x)\nXP Goal: {me.MentionedDSUser.XpGoal}{(me.MentionedDSUser.GlobalUser.IsPremium ? $"\n\n{me.Channel.Guild.Members[user.Id].DisplayName} is a premium user!" : "")}");
                return null;
            }
        };
    }

    //lists top 5 members in terms of level and xp
    public static Command TopCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "top",
            Category = "Fun",
            Description = "Top members of the server",
            action = async (me, ce) =>
            {
                //get users and sort
                var scoreboard = "";
                var susers = me.Server.DServerUsers.Where(x => !x.GlobalUser.IsBot)
                    .OrderByDescending(x => x.XpLevel).ThenByDescending(x => x.Xp)
                    .ToList();
                //add top 5 to message
                foreach (var user in susers.Take(5))
                    scoreboard +=
                        $"{susers.IndexOf(user) + 1}. {user.GlobalUser.Username}#{user.GlobalUser.Discriminator}\n" +
                        $"\tLevel: {user.XpLevel}, XP: {user.Xp}/{user.XpGoal}\n";

                await me.Channel.SendMessageAsync($"Top 5 users in *{me.Channel.Guild.Name}*:\n\n```{scoreboard}```");
                return null;
            }
        };
    }

    //leaderboard, sends a link to the leaderboard page for the current server
    public static Command LeaderboardCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "lb",
            Category = "Leveling",
            Description = "Leaderboards",
            action = async (me, ce) =>
            {
                await me.Channel.SendMessageAsync(
                    $"Leaderboard: <https://{bot.Bot.Name}.thearcanebrony.net/leaderboards/{me.Server.DiscordServerId}>");
                return null;
            }
        };
    }
}