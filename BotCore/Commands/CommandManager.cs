using System.Diagnostics;
using ArcaneLibs.Logging;
using BotCore.Classes;
using BotCore.Commands.Commands.Groups;
using BotCore.DbExtras;

namespace BotCore.Commands;

public class CommandManager
{
    public BotImplementation bot;

    public List<Command> Commands = new();

    public LogManager log = new()
    {
        LogTime = true
    };

    public CommandManager(BotImplementation bot)
    {
        this.bot = bot;
    }

    public bool CheckIfCommand(string message)
    {
        return message.StartsWith(bot.Bot.DefaultPrefix);
    }


    public async Task Execute(MessageEnvironment me)
    {
        if (!me.Message.Content.StartsWith(me.Server.Prefix) || me.Message.Author == bot.Bot.BotId) return;
        var ce = new CommandEnvironment
        {
            Args = me.Message.Content.Split(" ").Skip(1).ToList()
        };
        var cmdn = me.Message.Content.Split(" ")[0].Replace(me.Server.Prefix, "");
        //check if command exists
        if (Commands.Any(x => x.Name == cmdn))
        {
            var cmd = Commands.First(x => x.Name == cmdn);
            ce.Command = cmd;
            //await me.Channel.SendMessageAsync(JsonConvert.SerializeObject(cmd));
            cmd.Execute(me, ce);
            //check if an error happened
            if (ce.CommandStatus.Errored)
            {
                await me.Channel.SendMessageAsync("An error ocurred! see console for more info");
                //log all errors
                foreach (var commandStatusException in ce.CommandStatus.Exceptions)
                {
                    Console.WriteLine(commandStatusException);
                    if (Debugger.IsAttached) Debugger.Break();
                }
            }
        }
        //command doesnt exist
        else
        {
            if (me.Server.UnknownCommandMessageEnabled ?? true) await me.Channel.SendMessageAsync("Invalid command!");
        }

        log.LogSplit(" | ",
            parts: new object[]
            {
                me.Channel.Guild, me.Channel, me.Message.Author, me.Message,
                ce.CommandStatus == null ? "" : ce.CommandStatus.TimeSpent
            });
    }

    //register all base commands
    public void Init()
    {
        log.Prefix = $"{bot.Bot.FriendlyName}/Command Manager";
        var sw = new Stopwatch();
        sw.Start();
        log.Log("Setting up commands...");

        foreach (var type in new[]
                 {
                     typeof(CoreCommands),
                     typeof(DevCommands),
                     typeof(EconomyCommands),
                     typeof(FunCommands),
                     typeof(LevelCommands),
                     // typeof(ModCommands),
                     typeof(MusicCommands),
                     typeof(OwnerCommands),
                     typeof(TicketCommands)
                 })
        {
            RegisterCommandsInType(type);
        }

        log.Log($"Finished setting up commands! (Registered {Commands.Count} commands in {sw.ElapsedMilliseconds} ms)");
        sw.Stop();
    }

    //register all commands in a class
    public void RegisterCommandsInType(Type type)
    {
        log.Log($"Registering commands in {type.FullName}");
        //get all commands
        var methods = type.GetMethods().Where(x => x.ReturnType == typeof(Command));
        foreach (var method in methods)
        {
            //register
            log.LogDebug($"Registering {method.Name} in {type.FullName}");
            var cd = method.Invoke(type, new object?[] {bot}) as Command;
            RegisterCommand(cd);
        }
    }

    //register a command
    public void RegisterCommand(Command? def)
    {
        if (Commands.Any(x => x.Name == def.Name))
        {
            log.Log($"Command {def.Name} already exists!");
        }
        else
        {
            Commands.Add(def);
            log.LogDebug($"Added command {"[" + def.Category + ']',-12} {def.Name,-16} ({def.Description})");
        }
    }

    //register commands from a list
    public void RegisterCommands(IEnumerable<Command?> commands)
    {
        foreach (var cmd in commands) RegisterCommand(cmd);
    }
}