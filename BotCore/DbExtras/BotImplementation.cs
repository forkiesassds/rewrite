using System.Diagnostics;
using ArcaneLibs.Logging;
using BotCore.Commands;
using BotCore.DataModel;
using BotCore.Handlers.Message;
using BotCore.Handlers.Reaction;
using BotCore.Util.Systemd;
using DSharpPlus;
using DSharpPlus.EventArgs;
using DSharpPlus.VoiceNext;

namespace BotCore.DbExtras;

public class BotImplementation
{
    //loggers
    public LogManager AlertLog = new();
    public LogManager SysLog = new();
    public LogManager EventLog = new();
    
    public Bot Bot;
    public CommandManager CommandManager;
    public Db Db;
    public DiscordClient DiscordClient;
    public MessageHandler MessageHandler;
    public QuoteHandler QuoteHandler;

    public BotImplementation(Db db,Bot bot)
    {
        //start tracking time
        var transaction = Util.Sentry.StartTransaction("botcore_init");
        Console.WriteLine($"Initialising as {bot.FriendlyName}...");
        Bot = bot;
        bot.Db = db;
        Db = db;
        //initialise new handlers
        CommandManager = new CommandManager(this);
        MessageHandler = new MessageHandler(this);
        QuoteHandler = new QuoteHandler(this);
        //configure discord
        DiscordClient = new DiscordClient(new DiscordConfiguration
        {
            Intents = DiscordIntents.All,
            Token = bot.Token,
            AutoReconnect = true
        });
        DiscordClient.MessageCreated += MessageHandler.HandleMessage;
        DiscordClient.ThreadCreated += async (_, eventArgs) =>
        {
            await eventArgs.Thread.JoinThreadAsync();
        };
        DiscordClient.ClientErrored += (_, eventArgs) =>
        {
            Console.WriteLine($"An exception occured in {eventArgs.EventName}:\n```cs\n{eventArgs.Exception}\n```");
            return Task.CompletedTask;
        };
        DiscordClient.UseVoiceNext(new()
        {
            AudioFormat = AudioFormat.Default,
            PacketQueueSize = 10
        });
        DiscordClient.Ready += (sender, args) =>
        {
            Systemd.NotifyReady();
            EventLog.Log($"{Bot.FriendlyName} started up on {Environment.MachineName + (Debugger.IsAttached ? " (Debugging)" : "")}.");
            return Task.CompletedTask;
        };
        DiscordClient.MessageReactionAdded += QuoteHandler.ReactionAdded;
        DiscordClient.MessageReactionRemoved += QuoteHandler.ReactionRemoved;
        //load commands
        Console.WriteLine($"Loading commands for {bot.FriendlyName}...");
        CommandManager.Init();
        transaction.Finish();
        //log in
        Console.WriteLine($"Connecting as {Bot.FriendlyName}...");
        transaction = Util.Sentry.StartTransaction("botcore_connect");
        DiscordClient.ConnectAsync().GetAwaiter().GetResult();
        transaction.Finish();
    }

}