using System.Diagnostics;

namespace BotCore.Util.Audio;

public class AudioDecoder
{
    public static string CacheDir
    {
        get
        {
            var CacheDir = "./";
            while (!File.Exists(CacheDir + "DiscordBots.sln"))
            {
                CacheDir += "../";
                if (CacheDir.Length >= 50) //folder doesnt exist in 24 parent dirs
                {
                    throw new ArgumentNullException("Couldnt find root dir!");
                }
            }

            CacheDir += "cache/audio/";
            return new DirectoryInfo(CacheDir).FullName;
        }
    }

    public static Stream GetPCM(string id)
    {
        Directory.CreateDirectory(CacheDir + "pcm");
        Directory.CreateDirectory(CacheDir + "dl");

        if (!File.Exists($"{CacheDir}pcm/{id}.wav"))
        {
            var psi = new ProcessStartInfo
            {
                FileName = "ffmpeg",
                Arguments = $@"-i {CacheDir}dl/{id} -ar 48000 -f wav {CacheDir}pcm/{id}.wav",
                RedirectStandardOutput = false,
                UseShellExecute = false,
                RedirectStandardInput = false
            };
            var ffmpeg = Process.Start(psi);
            ffmpeg.WaitForExit();
        }

        return File.OpenRead(CacheDir + "pcm/" + id + ".wav");
    }
}