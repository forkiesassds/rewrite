using BotCore.DataModel;

namespace BotCore.Util.DbExtensions;

public static class DbExtensions
{
    public static Bot GetBot(this Db db, string name)
    {
        Sentry.InitSentry(name);
        var transaction = Util.Sentry.StartTransaction("fetch_bot_db");
        var bot = db.Bots.FirstOrDefault(x => x.Name == name);
        if (bot != null)
        {
            var cd = Environment.CurrentDirectory;
            while (!Directory.GetFiles(cd).Any(x => x.EndsWith(".csproj")) && cd.Length < 100) cd += "/..";
            bot.RelativePath = $"Bots/{new DirectoryInfo(cd).FullName.Split("/Bots/")[1]}";
            if (!bot.Enabled)
            {
                Console.WriteLine($"Bot \"{name}\" not enabled in database!");
                if (File.Exists($"/etc/systemd/system/botcore.bot.{name}.service"))
                {
                    File.Delete($"/etc/systemd/system/botcore.bot.{name}.service");
                    ArcaneLibs.Util.RunCommandSync("systemctl", "daemon-reload");
                    ArcaneLibs.Util.RunCommandSync("systemctl", $"disable --now botcore.bot.{name}");
                }
                transaction.Finish();
                throw new NullReferenceException(
                    $"Bot not enabled in database: {name}! Deleted systemd unit if it existed!");
            }
            //set version
            bot.CoreVer = VersionUtils.GetLongVersion();
            
            db.SaveChanges();
            //Download image repo if it doesn't exist
            ImageRepo.EnsureExists();
            transaction.Finish();
            return bot;
        }

        Console.WriteLine($"No bot with name \"{name}\"");
        throw new NullReferenceException($"No such bot: {name}!");
        transaction.Finish();
        return bot;
    }
    //get user
    public static GlobalUser GetGlobalUser(this Db db, ulong discordid)
    {
        var usr =
            db.GlobalUsers.FirstOrDefault(x => x.DiscordUserId == discordid.ToString());
        if (usr == null)
        {
            db.GlobalUsers.Add(usr = new GlobalUser
            {
                DiscordUserId = discordid.ToString()
            });
        }
        return usr;
    }

    //get server
    public static Server GetServer(this Db db, Bot bot, ulong guildid)
    {
        var srv = db.Servers.FirstOrDefault(
            x => x.DiscordServerId == guildid.ToString() && x.BotId == bot.BotId);
        if (srv == null)
        {
            db.Servers.Add(srv = new Server
            {
                DiscordServerId = guildid.ToString(),
                Bot = bot,
                Prefix = bot.DefaultPrefix
            });
        }
        return srv;
    }

    //get server user
    public static DServerUser GetServerUser(this Db db, Server server, GlobalUser globalUser)
    {
        var usr =
            db.DServerUsers.FirstOrDefault(x =>
                x.DServer == server && x.GlobalUser == globalUser);
        if (usr == null)
        {
            db.DServerUsers.Add(usr = new DServerUser
            {
                DServer = server,
                GlobalUser = globalUser
            });
        }
        return usr;
    }
}