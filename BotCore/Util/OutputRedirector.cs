using System.Text;

namespace BotCore.Util;

public class OutputRedirector : TextWriter
{
    public override Encoding Encoding => Encoding.Default;

    public event EventHandler<WriteEventArgs> WriteOcurred = (_, _) => { };

    public virtual void OnWrite(WriteEventArgs e)
    {
        var handler = WriteOcurred;
        handler.Invoke(this, e);
    }

    public override void Write(string value)
    {
        // base.Write(value);
        if (value != "\n") OnWrite(new WriteEventArgs {Text = value});
    }
}

public class WriteEventArgs
{
    public string Text = "no text provided";
}