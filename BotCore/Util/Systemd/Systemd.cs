using System.Runtime.InteropServices;

namespace BotCore.Util.Systemd;

public class Systemd
{
    [DllImport("libsystemd.so.0")]
    public static extern int sd_notify(int unset_environment, string state);

    public static void NotifyReady()
    {
        try
        {
            sd_notify(0, "READY=1");
        }
        catch (Exception ex)
        {
            // do something because we're really boned.
            Console.WriteLine("Failed to SD_NOTIFY");
        }
    }
}