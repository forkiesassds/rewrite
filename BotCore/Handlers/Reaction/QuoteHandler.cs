using BotCore.DataModel;
using BotCore.DbExtras;
using BotCore.Util.DbExtensions;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;

namespace BotCore.Handlers.Reaction;

public class QuoteHandler
{
    private readonly BotImplementation _bot;

    public QuoteHandler(BotImplementation bot)
    {
        _bot = bot;
    }
    
    public Task ReactionAdded(DiscordClient sender, MessageReactionAddEventArgs e)
    {
        UpdateQuote(e.Channel.GetMessageAsync(e.Message.Id).Result);
        return Task.CompletedTask;
    }

    public Task ReactionRemoved(DiscordClient sender, MessageReactionRemoveEventArgs e)
    {
        UpdateQuote(e.Channel.GetMessageAsync(e.Message.Id).Result);
        return Task.CompletedTask;
    }

    public void UpdateQuote(DiscordMessage msg)
    {
        var db = Db.GetNewPostgres();
        var quote = db.Quotes.FirstOrDefault(x => x.ChannelId == msg.ChannelId && x.MessageId == msg.Id);
        var bot = db.Bots.First(x => x.BotId == _bot.Bot.BotId);
        var srv = db.GetServer(bot, msg.Channel.GuildId.Value);
        if (quote == null)
            db.Quotes.Add(quote = new()
            {
                MessageText = msg.Content,
                ChannelId = msg.ChannelId,
                MessageId = msg.Id,
                Server = srv,
                Author = db.GetServerUser(srv, db.GetGlobalUser(msg.Author.Id))
            });

        var count = msg.Reactions.Sum(x => x.Count);
        Console.WriteLine(count);
        quote.Rating = count;
        db.SaveChanges();
        db.Dispose();
    }
}