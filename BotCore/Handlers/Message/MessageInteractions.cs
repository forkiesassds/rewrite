﻿using BotCore.Classes;
using DSharpPlus.Entities;

namespace BotCore.Handlers.Message;

public class MessageInteractions
{
    public static async void CheckMessage(MessageEnvironment me)
    {
        if (me.Message.Content.ToLower().StartsWith("poll: "))
        {
            await me.Message.CreateReactionAsync(DiscordEmoji.FromName(me.Bot.DiscordClient, ":white_check_mark:"));
            await me.Message.CreateReactionAsync(DiscordEmoji.FromName(me.Bot.DiscordClient, ":negative_squared_cross_mark:"));
            await me.Message.CreateReactionAsync(DiscordEmoji.FromName(me.Bot.DiscordClient, ":grey_question:"));
        }
    }
}