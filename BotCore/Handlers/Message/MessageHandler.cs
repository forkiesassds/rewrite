using ArcaneLibs.Logging;
using ArcaneLibs.Logging.LogEndpoints;
using BotCore.Classes;
using BotCore.DataModel;
using BotCore.DbExtras;
using BotCore.Util.DbExtensions;
using DSharpPlus;
using DSharpPlus.EventArgs;

namespace BotCore.Handlers.Message;

public class MessageHandler
{
    public BotImplementation bot;

    public LogManager log = new()
    {
        LogTime = true
    };

    public MessageHandler(BotImplementation bot)
    {
        this.bot = bot;
        log.Prefix = $"{bot.Bot.FriendlyName}/";
        log.AddEndpoint(new ConsoleEndpoint());
    }

    public Task HandleMessage(DiscordClient sender, MessageCreateEventArgs msg)
    {
        var transaction = Util.Sentry.StartTransaction("handle_message");
        //var scope = SentrySdk.PushScope();
        var environment = new MessageEnvironment
        {
            Bot = bot,
            Message = msg.Message,
            Channel = msg.Channel,
            Db = bot.Db
        };
        //TODO: fix defaults
        //get user
        environment.User = environment.Db.GetGlobalUser(environment.Message.Author.Id);
        //get server
        environment.Server = environment.Db.GetServer(environment.Bot.Bot, environment.Channel.GuildId.Value);
        //get server user
        environment.DSUser = environment.Db.GetServerUser(environment.Server, environment.User);
        //fetch data for first mentioned user, if any, otherwise just use author
        environment.MentionedDSUser = environment.DSUser;
        if (environment.Message.MentionedUsers.Count > 0)
        {
            //get user
            environment.MentionedUser = environment.Db.GetGlobalUser(environment.Message.MentionedUsers[0].Id);
            //get server user
            environment.MentionedDSUser = environment.Db.GetServerUser(environment.Server, environment.MentionedUser);
        }
        //handle xp and interactions
        Xp.CheckMessage(environment);
        MessageInteractions.CheckMessage(environment);

        //handle commands
        if (environment.Message.Content.StartsWith(environment.Server.Prefix))
        {
            var Span = transaction.StartChild("CommandHandling");
            bot.CommandManager.Execute(environment);
            Span.Finish();
        }

        log.Log($"Finished handling message: {environment.Message}");
        //update user and guild info
        UpdateInfo(environment);
        //save changes if any
        environment.Db.SaveChanges();
        //scope.Dispose();
        transaction.Finish();
        return null;
    }

    private void UpdateInfo(MessageEnvironment env)
    {
        //guild
        if (env.Channel.Guild != null)
        {
            env.Server.Name = env.Channel.Guild.Name;
            env.Server.IconUrl = env.Channel.Guild.GetIconUrl(ImageFormat.Auto) ??
                             "https://cdn.discordapp.com/embed/avatars/0.webp?size=512";
            env.Server.MemberCount = env.Channel.Guild.MemberCount;
        }
        //member
        env.User.Username = env.Message.Author.Username;
        env.User.Discriminator = env.Message.Author.Discriminator;
        env.User.AvatarUrl = env.Message.Author.GetAvatarUrl(ImageFormat.Auto) ??
                          "https://cdn.discordapp.com/embed/avatars/0.webp?size=512";
        env.User.IsBot = env.Message.Author.IsBot;
        env.User.IsPremium = DateTime.Now <= env.User.PremiumSince + env.User.PremiumDuration;
    }
}