using BotCore.DataModel;
using BotCore.DbExtras;

namespace BotCore.Monitoring;

public class Monitor
{
    public static void StartMonitoring(BotImplementation bot)
    {
        new Thread(() =>
        {
            while (true)
            {
                var drv = DriveInfo.GetDrives().Where(x => Environment.CurrentDirectory.StartsWith(x.Name))
                    .MaxBy(x => x.Name.Length);
                if (drv.AvailableFreeSpace <= 5_000_000_000)
                {
                    bot.AlertLog.Log($"Disk alert! Partition {drv.Name} on {Environment.MachineName} has {ArcaneLibs.Util.BytesToString(drv.AvailableFreeSpace)} free space remaining!");
                }
                Thread.Sleep(TimeSpan.FromMinutes(5));
            }
        }).Start();
    }
}