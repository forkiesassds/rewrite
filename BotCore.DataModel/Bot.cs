﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BotCore.DataModel;

[Table("bot")]
public class Bot
{
    //runtime data
    [NotMapped] public Db? Db;

    [Key] public int BotId { get; set; }

    //properties
    public string Name { get; set; } = null!;
    public string FriendlyName { get; set; } = null!;

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public string? Token { get; set; }

    public int ServerCount { get; set; } = 0;
    public int MemberCount { get; set; } = 0;
    public int OnlineMemberCount { get; set; } = 0;
    public string AvatarUrl { get; set; } = "https://discord.com/assets/3c6ccb83716d1e4fb91d3082f6b21d77.png";
    public string CoreVer { get; set; } = "v4.0.0a0";
    public string DefaultPrefix { get; set; } = "/";
    public bool Enabled { get; set; } = true;
    public bool RemotelyDeployed { get; set; } = false;
    public string RelativePath { get; set; } = "";

    //relationsd
    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public List<Server> Servers { get; set; } = new();
}