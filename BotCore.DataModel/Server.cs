﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BotCore.DataModel;

[Table("server")]
public class Server
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long ServerId { get; set; }

    //properties
    public string DiscordServerId { get; set; }
    public int BotId { get; set; }
    public int MemberCount { get; set; } = 0;
    public int OnlineMemberCount { get; set; } = 0;
    public string IconUrl { get; set; } = "https://cdn.discordapp.com/embed/avatars/0.png";
    public string Name { get; set; } = "Unknown Server";
    public bool AutoroleEnabled { get; set; } = false;
    public bool? LevelsEnabled { get; set; } = true;
    public bool LockdownEnabled { get; set; } = false;
    public int MessageCount { get; set; } = 0;
    public bool? ModerationEnabled { get; set; } = false;
    public bool? LevelupMessagesEnabled { get; set; } = true;
    public bool? RaidDefenseEnabled { get; set; } = false;
    public bool ResetOnLeave { get; set; } = false;
    public bool? DisabledCommandMessageEnabled { get; set; } = true;
    public int MinLevelForLeaderboard { get; set; } = 2;
    public bool? UnknownCommandMessageEnabled { get; set; } = true;
    public bool ContentFilterEnabled { get; set; }
    public bool MentionsEnabled { get; set; }
    public int MentionTreshold { get; set; } = 7;
    public bool InvitesEnabled { get; set; }
    public int InviteTreshold { get; set; } = 1;
    public bool EmotesEnabled { get; set; }
    public int EmoteTreshold { get; set; } = 10;
    public bool BadLangEnabled { get; set; }
    public int BadLangTreshold { get; set; } = 10;
    public string Prefix { get; set; } = "<<";
    public List<string> DisabledCommands { get; set; } = new();
    public List<string> WordFilter { get; set; } = new();
    public List<string> BannedNameParts { get; set; } = new();
    public List<string> IgnoredChannels { get; set; } = new();

    //relations
    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public Bot Bot { get; set; } = null!;

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public List<DServerUser> DServerUsers { get; set; }

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public List<Quote> Quotes { get; set; }
}