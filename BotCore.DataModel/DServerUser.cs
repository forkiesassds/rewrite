﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BotCore.DataModel;

[Table("d_server_user")]
public class DServerUser
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long UserId { get; set; }

    //properties
    public int MessageCount { get; set; } = 0;
    public int Xp { get; set; } = 0;

    [NotMapped] public int XpGoal => (XpLevel * 100 + 10) * 25;

    [NotMapped] public double XpProgress => Xp / (double) XpGoal;

    public int XpLevel { get; set; } = 0;
    public double XpMultiplier { get; set; } = 1;
    public bool Muted { get; set; } = false;

    [Column(TypeName = "timestamp without time zone")]
    public DateTime UnmuteBy { get; set; } = DateTime.Now;

    public bool Left { get; set; } = false;
    public long BlockedWordCount { get; set; } = 0;
    public double EmoteCount { get; set; } = 0;
    public double InviteCount { get; set; } = 0;
    public double MentionCount { get; set; } = 0;
    public double BadLanguageCount { get; set; } = 0;

    public List<string>? Nicknames { get; set; } = new();

    //relations
    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public Server DServer { get; set; } = null!;

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public GlobalUser GlobalUser { get; set; } = null!;

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public List<Quote> Quotes { get; set; } = new();

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    [InverseProperty("User")]
    public List<Warning> Warnings { get; set; } = new();

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    [InverseProperty("Author")]
    public List<Warning> AuthoredWarnings { get; set; } = new();
}