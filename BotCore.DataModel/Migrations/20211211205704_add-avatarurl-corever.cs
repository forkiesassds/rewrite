﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BotCore.DataModel.Migrations
{
    public partial class addavatarurlcorever : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AvatarUrl",
                table: "bot",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "CoreVer",
                table: "bot",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AvatarUrl",
                table: "bot");

            migrationBuilder.DropColumn(
                name: "CoreVer",
                table: "bot");
        }
    }
}
