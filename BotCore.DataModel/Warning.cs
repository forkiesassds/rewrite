﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BotCore.DataModel;

[Table("warning")]
public class Warning
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long WarningId { get; set; }

    public long? AuthorId { get; set; }
    public long? UserId { get; set; }

    //properties
    [StringLength(2000)] public string Message { get; set; } = "";

    //relations

    [ForeignKey("AuthorId")]
    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public DServerUser? Author { get; set; }

    [ForeignKey("UserId")]
    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public DServerUser? User { get; set; }
}