﻿using ArcaneLibs.Logging;
using ArcaneLibs.Logging.LogEndpoints;
using BotCore.DataModel;
using BotCore.DataUpdater;
using BotCore.Util.Systemd;
using DSharpPlus;
using Microsoft.EntityFrameworkCore;

Systemd.NotifyReady();
var cfg = DbConfig.Read();
cfg.Save();
var qrylog = new LogManager
{
    LogTime = true
};
qrylog.AddEndpoint(new FileEndpoint("qry.log", true));
Db.GetNewPostgres().Dispose();
// if(Debugger.IsAttached) qrylog.AddEndpoint(new ConsoleEndpoint());
var db = Db.GetNewPostgres();
var db2 = Db.GetNewPostgres();

//ensure db exists and is up to date
db.Database.Migrate();
db.SaveChanges();

//remove empty quotes
var deadquotes = db.Quotes.Where(x => x.Rating == 0).ToList();
db.Quotes.RemoveRange(deadquotes);
Console.WriteLine($"Deleted {deadquotes.Count} quotes without ratings.");

//check and fix database consistency
DbConsistency.Execute(db);

//run all bots
var ready = 0;
var bcount = 0;
List<DiscordClient> cl = new();
db.Bots.Where(x => x.Token.Length < 2 && x.Enabled).ToList().ForEach(x => x.Enabled = false);
foreach (var bot in db.Bots.Where(x => x.Token != null && x.Token.Length >= 2 && x.Enabled).ToList())
{
    bcount++;
    DiscordClient c;
    cl.Add(c = await BotRunner.RunBot(db, bot));
    var AvailableGuilds = 0;
    c.GuildDownloadCompleted += async (_, _) => { ready++; };
    c.Resumed += async (_, _) =>
    {
        Console.WriteLine("We resumed, we shouldn't be running for this long! Exiting...");
        Environment.Exit(0);
    };
    Console.WriteLine(bot.Name + " starting...");
    await c.ConnectAsync();
}

//wait until all server/bot data is up to date
while (ready < bcount) Thread.Sleep(1000);
db.SaveChanges();

//ensure member counts are up to date
foreach (var bot in db.Bots.Where(x => x.Token.Length >= 2))
{
    bot.OnlineMemberCount = bot.Servers.Sum(x => x.OnlineMemberCount);
    bot.MemberCount = bot.Servers.Sum(x => x.MemberCount);
}

db.SaveChanges();

//update usernames, discriminators and avatars
await AvatarUpdater.Run(cl, db);
db.SaveChanges();
db2.SaveChanges();

Console.WriteLine("Updating data complete, exiting!");
//application doesn't exit due to running bots, force exit
Environment.Exit(0);