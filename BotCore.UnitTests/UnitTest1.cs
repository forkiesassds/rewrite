using System;
using System.Linq;
using BotCore.DataModel;
using BotCore.Util;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Xunit;
using Xunit.Abstractions;

namespace BotCore.UnitTests;

public class UnitTests
{
    private static ITestOutputHelper? testOutputHelper;
    private readonly Db db;

    public UnitTests(ITestOutputHelper _testOutputHelper)
    {
        //redirect console output
        testOutputHelper = _testOutputHelper;
        var output = new OutputRedirector();
        output.WriteOcurred += (_, args) => { _testOutputHelper.WriteLine(args.Text.TrimEnd('\n').TrimEnd('\r')); };
        Console.SetOut(output);

        //log into db
        var cfg = DbConfig.Read();
        cfg.Save();
        db = new Db(new DbContextOptionsBuilder<Db>()
            .UseNpgsql(
                $"Host={cfg.Host};Database={cfg.Database};Username={cfg.Username};Password={cfg.Password};Port={cfg.Port};Include Error Detail=true")
            .LogTo(Console.WriteLine, LogLevel.Information).EnableSensitiveDataLogging().Options);
        db.Database.Migrate();
        db.SaveChanges();
        while (db.GlobalUsers.Count() < 10)
        {
        }
    }

    [Fact]
    public void Test1()
    {
        Console.Write("hmm... ");
        Console.WriteLine("hmm?");
    }

    [Fact(DisplayName = "Check database for invalid data")]
    public void CheckDbConsistency()
    {
        //friendly names set
        Assert.True(db.Bots.All(x => x.FriendlyName != null && x.FriendlyName != ""));
        //no negative xp
        Assert.True(!db.DServerUsers.Any(x => x.Xp < 0 || x.XpLevel < 0));
    }
}