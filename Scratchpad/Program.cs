﻿// See https://aka.ms/new-console-template for more information

using System.Text.Json.Serialization;
using Newtonsoft.Json;

Console.WriteLine("Hello, World!");
foreach (var driveInfo in DriveInfo.GetDrives())
{
    if(driveInfo.TotalSize != 0)
    Console.WriteLine($"{driveInfo.Name}: {Math.Round(driveInfo.TotalFreeSpace/(double)driveInfo.TotalSize*100)}% free");
}